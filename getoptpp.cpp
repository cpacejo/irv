#include <initializer_list>
#include <string>
extern "C"
{
//#include <getopt.h>
#include <unistd.h>
}
#include "getoptpp.h"

ctp::getopt::iterator& ctp::getopt::iterator::operator++()
{
  value.opt = ::getopt(getopt->argc, getopt->argv, getopt->optstring.c_str());
  value.ind = optind;
  value.arg = optarg;
  return *this;
}

ctp::getopt::iterator ctp::getopt::iterator::operator++(int)
{
  const iterator ret = *this;
  ++*this;
  return ret;
}

ctp::getopt::iterator::iterator(const class getopt* const getopt) noexcept
  : getopt(getopt)
{
  value.opt = ::getopt(getopt->argc, getopt->argv, getopt->optstring.c_str());
  value.ind = optind;
  value.arg = optarg;
}

ctp::getopt::getopt(const int argc, char* argv[],
    std::string optstring,
    const std::initializer_list<option>)
  : argc(argc), argv(argv), optstring(std::move(optstring))
{
}

ctp::getopt::iterator ctp::getopt::begin() const
{
  ::optind = 1;
  return iterator(this);
}
