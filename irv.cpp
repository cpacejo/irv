#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <iterator>
#include <map>
#include <sstream>
#include <string>
#include <tuple>
#include <utility>
#include <vector>
#include "fraction.h"
#include "getoptpp.h"

// TODO:
// * help
// * tiebreaking via Random Voter Hierarchy
// * tests!

namespace
{
  bool verbose = false;

  using name = std::string;

  struct vote;

  struct candidate
  {
    bool eliminated = false;
    ctp::fraction<> vote_count = 0;
    std::vector<std::pair<vote*, ctp::fraction<>>> votes;
  };

  struct vote
  {
    std::deque<std::pair<const name, candidate>*> ranking;
  };

  template<typename T>
  std::ostream& operator<<(std::ostream& os, const ctp::fraction<T>& frac)
  {
    ctp::fraction<T> intpart;
    ctp::fraction<T> fracpart = modf(frac, &intpart);
    if (intpart || !fracpart) os << intpart.num();
    if (intpart && fracpart) os << "+";
    if (fracpart) os << fracpart.num() << "/" << fracpart.den();
    return os;
  }

  std::pair<std::map<name, candidate>, std::vector<vote>> read_votes()
  {
    std::pair<std::map<name, candidate>, std::vector<vote>> ret;
    std::map<name, candidate>& candidates = ret.first;
    std::vector<vote>& votes = ret.second;

    std::string line;
    while (std::getline(std::cin, line))
    {
      votes.emplace_back();
      auto& ranking = votes.back().ranking;

      // Split on whitespace.
      bool blank = true;
      std::istringstream iss(line);
      std::string name;
      while (iss >> name)
      {
        // "*" represents an intentionally spoiled vote.
        // (These are not 100% equivalent to voting for an out-of-bound
        // candidate, since there must be strictly *more* than the winning
        // threshold worth of spoiling votes to result in no winner.)
        if (name != "*")
          ranking.push_back(&*candidates.emplace(std::move(name), candidate()).first);

        // Even spoiled votes count as a vote.
        blank = false;
      }

      // Ignore blank lines (but not spoiled ballots).
      if (blank) votes.pop_back();
    }

    if (!std::cin.eof())
    {
      std::cerr << "ERROR: Error reading votes.\n";
      std::exit(3);
    }

    return ret;
  }

  void redistribute_votes(const name& n, candidate* const c, const ctp::fraction<> new_vote_count = 0)
  {
    assert(c->eliminated);
    assert(new_vote_count <= c->vote_count);

    if (!c->vote_count)
    {
      assert(c->votes.empty());
      return;
    }

    const ctp::fraction<> excess_ratio = (c->vote_count - new_vote_count) / c->vote_count;

    if (verbose)
    {
      if (new_vote_count)
        std::cerr << "Transferring " << (c->vote_count - new_vote_count) << " vote ("
          << excess_ratio << ") of " << c->vote_count << " total from candidate " << n
          << " to the following candidates:\n";
      else
        std::cerr << "Transferring all " << c->vote_count << " votes from candidate " << n
          << " to the following candidates:\n";
    }

    std::map<name, ctp::fraction<> > transferred;
    ctp::fraction<> extinguished = 0;

    for (auto& [v, r]: c->votes)
    {
      while (!v->ranking.empty() && v->ranking.front()->second.eliminated)
        v->ranking.pop_front();

      if (v->ranking.empty())
      {
        if (verbose) extinguished += excess_ratio * r;
      }
      else
      {
        candidate& d = v->ranking.front()->second;
        d.vote_count += excess_ratio * r;
        d.votes.emplace_back(v, excess_ratio * r);
        if (verbose)
          transferred.emplace(v->ranking.front()->first, 0).first->second += excess_ratio * r;
      }
    }

    // Update count for recordkeeping purposes; the algorithm
    // no longer needs it.
    c->vote_count = new_vote_count;

    // These aren't needed any more; save memory.
    c->votes.clear();
    c->votes.shrink_to_fit();

    if (verbose)
    {
      for (auto& [n, v]: transferred)
        std::cerr << n << ": +" << v << "\n";
      if (extinguished)
        std::cerr << "[extinguished] " << extinguished << "\n";
      std::cerr << "\n";
    }
  }
}

int main(int argc, char* argv[])
{
  using std::begin;
  using std::end;

  int num_winners = 1;

  for (auto [opt, ind, arg]: ctp::getopt(argc, argv, "hw:V"))
  {
    switch (opt)
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]\n";
      std::cout << "  -h    this help\n";
      std::cout << "  -w #  set number of winners desired; default 1\n";
      std::cout << "  -V    print details of all rounds to standard error\n";
      std::cout << "\n";
      std::cout << "Pass ballots on standard input; one ballot per line.\n";
      std::cout << "Separate votes within each ballot with spaces.\n";
      std::cout << "Order votes within each ballot from most to least preferred.\n";
      std::cout << "Winners will be printed to standard output, one per line.\n";
      std::exit(0);
      break;

    case 'w':
      num_winners = std::atoi(arg);
      break;

    case 'V':
      verbose = true;
      break;
    }
  }

  if (num_winners < 1)
  {
    std::cerr << "ERROR: Must request at least 1 winner.\n";
    std::exit(2);
  }

  if (verbose)
    std::cerr << "Performing " << num_winners << "-winner instant runoff election.\n\n";

  std::vector<vote> votes;
  std::map<name, candidate> candidates;
  std::tie(candidates, votes) = read_votes();
  if (votes.empty())
  {
    std::cerr << "ERROR: No votes read.\n";
    std::exit(1);
  }

  if (verbose)
  {
    std::cerr << "Read " << votes.size() << " votes for " << candidates.size() << " candidates.\n\n";
    std::cerr << "Candidates are:\n";
    for (auto& [name, c]: candidates)
      std::cerr << name << "\n";
    std::cerr << "\n";
  }

  const ctp::fraction<> num_votes(votes.size());
  const ctp::fraction<> threshold(num_votes / ctp::fraction<>(num_winners + 1));

  if (verbose)
    std::cerr << "Winning threshold is " << threshold << " votes.\n\n";

  std::vector<std::pair<const name, candidate>*> winners;

  // Dole out initial votes.
  for (vote& v: votes)
  {
    if (!v.ranking.empty())
    {
      candidate& c = v.ranking.front()->second;
      ++c.vote_count;
      c.votes.emplace_back(&v, 1);
    }
  }

  // Ensure at least one non-spoiler vote cast.
  if (!std::any_of(begin(candidates), end(candidates),
      [](const std::pair<const name, candidate>& c) { return static_cast<bool>(c.second.vote_count); }))
  {
    std::cerr << "ERROR: No non-spoiler votes cast.\n";
    std::exit(1);
  }

  for (int round = 1;; ++round)
  {
    if (verbose)
    {
      std::cerr << "--- ROUND " << round << " ----\n\n";

      std::cerr << "Vote totals of remaining candidates:\n";
      for (auto& [name, c]: candidates)
        if (!c.eliminated)
          std::cerr << name << ": " << c.vote_count << "\n";
      std::cerr << "\n";
    }

    std::vector<std::pair<const name, candidate>*> new_winners;
  
    // Look for winners.
    for (auto& c: candidates)
      if (!c.second.eliminated && c.second.vote_count >= threshold)
        new_winners.push_back(&c);

    if (new_winners.empty())
    {
      // No winner found.  Eliminate losing candidates whose votes combined
      // cannot exceed either the threshold, or any other candidate, or,
      // if there is no such candidate, eliminate the least viable candidate.

      // First sort remaining candidates by current vote totals.
      std::vector<std::pair<const name, candidate>*> candidate_order;
      for (auto& n_c: candidates)
        if (!n_c.second.eliminated)
          candidate_order.push_back(&n_c);

      // FIXME: break ties correctly!
      std::sort(begin(candidate_order), end(candidate_order),
        [](std::pair<const name, candidate> *const a, std::pair<const name, candidate> *const b)
        { return a->second.vote_count < b->second.vote_count; });

      // Next, find unviable candidates as described above.
      auto first_viable = begin(candidate_order);
      ctp::fraction<> losing_total = 0;
      ctp::fraction<> unviable_total;
      for (auto it = begin(candidate_order); it != end(candidate_order); ++it)
      {
        if ((*it)->second.vote_count > losing_total)
        {
          first_viable = it;
          unviable_total = losing_total;
        }

        losing_total += (*it)->second.vote_count;
      }

      // Ensure that the remaining candidates have enough votes to pass the threshold.
      if (losing_total < threshold)
      {
        if (verbose)
          std::cerr << "Only " << losing_total << " votes remain; this is below the threshold to win.\n";

        break;
      }

      // At this point we know we have at least one remaining candidate.
      assert(!candidate_order.empty());

      // If we found no unviable candidates, choose the least viable to eliminate.
      if (first_viable == begin(candidate_order))
      {
        if (verbose)
          std::cerr << "Eliminating least viable candidate " << (*first_viable)->first << ".\n\n";

        ++first_viable;
      }
      else
      {
        if (verbose)
        {
          auto eliminated = std::vector(begin(candidate_order), first_viable);
          std::sort(begin(eliminated), end(eliminated),
            [](std::pair<const name, candidate> *const a, std::pair<const name, candidate> *const b)
            { return a->first < b->first; });

          std::cerr << "The following candidates have fewer votes in total (" << unviable_total
            << ") than any other single candidate (" << (*first_viable)->second.vote_count << ") and will be eliminated:\n";
          for (auto& n_c: eliminated)
            std::cerr << n_c->first << "\n";
          std::cerr << "\n";
        }
      }

      // Mark all unviable candidates as eliminated.
      std::for_each(begin(candidate_order), first_viable,
        [](std::pair<const name, candidate>* const c) { c->second.eliminated = true; });

      // Redistribute votes of all unviable candidates
      std::for_each(begin(candidate_order), first_viable,
        [](std::pair<const name, candidate>* const c) { redistribute_votes(c->first, &c->second); });
    }
    else
    {
      if (verbose)
      {
        std::cerr << "The following candidates passed the threshold to win:\n";
        for (auto it = begin(new_winners); it != end(new_winners); ++it)
          std::cerr << (*it)->first << "\n";
        std::cerr << "\n";
      }

      // Winner(s) found!  Add to list, and finish if we've found them all.
      std::copy(begin(new_winners), end(new_winners), std::back_inserter(winners));
      if (int(winners.size()) >= num_winners) break;

      // We need more winners; eliminate these winners
      // and redistribute excess votes to other candidates.
      // (Do this in two stages; else one winner's excess votes
      // may be added to another winner's total, and then
      // only a fraction of their next choice will count.)
      for (const auto& winner: new_winners)
        winner->second.eliminated = true;

      for (const auto winner: new_winners)
        redistribute_votes(winner->first, &winner->second, threshold);
    }
  }

  if (int(winners.size()) < num_winners)
    std::cerr << "ERROR: Insufficient number of candidates passed threshold to win.\n";

  if (int(winners.size()) > num_winners)
    std::cerr << "ERROR: Too many candidates met threshold to win.\n";

  if (verbose)
    std::cerr << "All winners have been identified.  Election has ended.\n";

  for (const auto winner: winners)
    std::cout << winner->first << std::endl;

  return int(winners.size()) == num_winners ? 0 : 1;
}
