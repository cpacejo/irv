#ifndef CTP_FRACTION_H
#define CTP_FRACTION_H

#include <cstdint>
#include <initializer_list>
#include <type_traits>

// TODO: mixed-rank arithmetic?
// TODO: remainder, remquo, nextafter?, nexttoward?
// TODO: infinities/NaN?

namespace ctp
{
  template<typename T> class fraction;

  template<typename T> constexpr fraction<T> operator+(const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> operator-(const fraction<T>&) noexcept;

  template<typename T> constexpr fraction<T> operator+(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> operator+(const fraction<T>&, T) noexcept;
  template<typename T> constexpr fraction<T> operator+(T, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> operator-(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> operator-(const fraction<T>&, T) noexcept;
  template<typename T> constexpr fraction<T> operator-(T, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> operator*(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> operator*(const fraction<T>&, T) noexcept;
  template<typename T> constexpr fraction<T> operator*(T, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> operator/(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> operator/(const fraction<T>&, T) noexcept;
  template<typename T> constexpr fraction<T> operator/(T, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> operator%(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> operator%(const fraction<T>&, T) noexcept;
  template<typename T> constexpr fraction<T> operator%(T, const fraction<T>&) noexcept;

  template<typename T> constexpr bool operator==(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr bool operator==(const fraction<T>&, T) noexcept;
  template<typename T> constexpr bool operator==(T, const fraction<T>&) noexcept;
  template<typename T> constexpr bool operator!=(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr bool operator!=(const fraction<T>&, T) noexcept;
  template<typename T> constexpr bool operator!=(T, const fraction<T>&) noexcept;
  template<typename T> constexpr bool operator<(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr bool operator<(const fraction<T>&, T) noexcept;
  template<typename T> constexpr bool operator<(T, const fraction<T>&) noexcept;
  template<typename T> constexpr bool operator<=(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr bool operator<=(const fraction<T>&, T) noexcept;
  template<typename T> constexpr bool operator<=(T, const fraction<T>&) noexcept;
  template<typename T> constexpr bool operator>(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr bool operator>(const fraction<T>&, T) noexcept;
  template<typename T> constexpr bool operator>(T, const fraction<T>&) noexcept;
  template<typename T> constexpr bool operator>=(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr bool operator>=(const fraction<T>&, T) noexcept;
  template<typename T> constexpr bool operator>=(T, const fraction<T>&) noexcept;

  template<typename T> constexpr fraction<T> abs(const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> fabs(const fraction<T>&) noexcept;
  template<typename T> fraction<T> modf(const fraction<T>&, fraction<T>* intpart) noexcept;
  template<typename T> constexpr fraction<T> ceil(const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> floor(const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> fmod(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> trunc(const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> round(const fraction<T>&) noexcept;
  template<typename T> constexpr long lround(const fraction<T>&) noexcept;
  template<typename T> constexpr long long llround(const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> copysign(const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> fma(const fraction<T>&, const fraction<T>&, const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> fdim(const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> fmax(const fraction<T>&) noexcept;
  template<typename T> constexpr fraction<T> fmin(const fraction<T>&) noexcept;

  template<typename T = std::intmax_t>
  class fraction
  {
  public:
    using value_type = typename std::enable_if<std::is_signed<T>::value, T>::type;

    constexpr fraction() noexcept = default;
    constexpr fraction(T num) noexcept;
    constexpr fraction(T num, T den) noexcept;
    template<typename U> constexpr fraction(const fraction<U>&) noexcept;

    constexpr T num() const noexcept;
    constexpr T den() const noexcept;

    constexpr explicit operator bool() const noexcept;
    constexpr explicit operator unsigned int() const noexcept;
    constexpr explicit operator unsigned long() const noexcept;
    constexpr explicit operator unsigned long long() const noexcept;
    constexpr explicit operator int() const noexcept;
    constexpr explicit operator long() const noexcept;
    constexpr explicit operator long long() const noexcept;
    constexpr explicit operator float() const noexcept;
    constexpr explicit operator double() const noexcept;
    constexpr explicit operator long double() const noexcept;

    constexpr fraction inverse() const noexcept;
    fraction& invert() noexcept;

    fraction& operator++() noexcept;
    fraction operator++(int) noexcept;
    fraction& operator--() noexcept;
    fraction operator--(int) noexcept;

    fraction& operator+=(T) noexcept;
    fraction& operator+=(const fraction&) noexcept;
    fraction& operator-=(T) noexcept;
    fraction& operator-=(const fraction&) noexcept;
    fraction& operator*=(T) noexcept;
    fraction& operator*=(const fraction&) noexcept;
    fraction& operator/=(T) noexcept;
    fraction& operator/=(const fraction&) noexcept;
    fraction& operator%=(T) noexcept;
    fraction& operator%=(const fraction&) noexcept;

    friend constexpr fraction operator+ <>(const fraction&) noexcept;
    friend constexpr fraction operator- <>(const fraction&) noexcept;
    friend constexpr fraction operator+ <>(const fraction&, const fraction&) noexcept;
    friend constexpr fraction operator+ <>(const fraction&, T) noexcept;
    friend constexpr fraction operator+ <>(T, const fraction&) noexcept;
    friend constexpr fraction operator- <>(const fraction&, const fraction&) noexcept;
    friend constexpr fraction operator- <>(const fraction&, T) noexcept;
    friend constexpr fraction operator- <>(T, const fraction&) noexcept;
    friend constexpr fraction operator* <>(const fraction&, const fraction&) noexcept;
    friend constexpr fraction operator* <>(const fraction&, T) noexcept;
    friend constexpr fraction operator* <>(T, const fraction&) noexcept;
    friend constexpr fraction operator/ <>(const fraction&, const fraction&) noexcept;
    friend constexpr fraction operator/ <>(const fraction&, T) noexcept;
    friend constexpr fraction operator/ <>(T, const fraction&) noexcept;
    friend constexpr bool operator== <>(const fraction&, const fraction&) noexcept;
    friend constexpr bool operator== <>(const fraction&, T) noexcept;
    friend constexpr bool operator== <>(T, const fraction&) noexcept;
    friend constexpr bool operator!= <>(const fraction&, const fraction&) noexcept;
    friend constexpr bool operator!= <>(const fraction&, T) noexcept;
    friend constexpr bool operator!= <>(T, const fraction&) noexcept;
    friend constexpr bool operator< <>(const fraction&, const fraction&) noexcept;
    friend constexpr bool operator< <>(const fraction&, T) noexcept;
    friend constexpr bool operator< <>(T, const fraction&) noexcept;
    friend constexpr bool operator<= <>(const fraction&, const fraction&) noexcept;
    friend constexpr bool operator<= <>(const fraction&, T) noexcept;
    friend constexpr bool operator<= <>(T, const fraction&) noexcept;
    friend constexpr bool operator> <>(const fraction&, const fraction&) noexcept;
    friend constexpr bool operator> <>(const fraction&, T) noexcept;
    friend constexpr bool operator> <>(T, const fraction&) noexcept;
    friend constexpr bool operator>= <>(const fraction&, const fraction&) noexcept;
    friend constexpr bool operator>= <>(const fraction&, T) noexcept;
    friend constexpr bool operator>= <>(T, const fraction&) noexcept;
    friend constexpr fraction abs<>(const fraction&) noexcept;
    friend constexpr fraction fabs<>(const fraction&) noexcept;
    friend fraction modf<>(const fraction&, fraction*) noexcept;
    friend constexpr fraction copysign<>(const fraction&, const fraction&) noexcept;

  protected:
    constexpr fraction(std::initializer_list<T>) noexcept;

  private:
    T m_num = T();
    T m_den = T(1);
  };
}


// PRIVATE

#include <cmath>
#include <cstdlib>
#include <iterator>
#include <numeric>
#include <utility>

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator+(const fraction<T>& x) noexcept
{ return x; }

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator-(const fraction<T>& x) noexcept
{
  return fraction<T>{-x.m_num, x.m_den};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator+(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{
  const T lcm = std::lcm(lhs.m_den, rhs.m_den);
  return fraction<T>(lhs.m_num * (lcm / lhs.m_den) + (lcm / rhs.m_den) * rhs.m_num, lcm);
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator+(const fraction<T>& lhs, const T rhs) noexcept
{
  return fraction<T>{lhs.m_num + lhs.m_den * rhs, lhs.m_den};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator+(const T lhs, const fraction<T>& rhs) noexcept
{ return rhs + lhs; }

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator-(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{
  const T lcm = std::lcm(lhs.m_den, rhs.m_den);
  return fraction<T>(lhs.m_num * (lcm / lhs.m_den) - (lcm / rhs.m_den) * rhs.m_num, lcm);
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator-(const fraction<T>& lhs, const T rhs) noexcept
{
  return fraction<T>{lhs.m_num - lhs.m_den * rhs, lhs.m_den};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator-(const T lhs, const fraction<T>& rhs) noexcept
{
  return fraction<T>{lhs * rhs.m_den - rhs.m_num, rhs.m_den};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator*(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{
  const T gcd1 = std::gcd(lhs.m_num, rhs.m_den);
  const T gcd2 = std::gcd(lhs.m_den, rhs.m_num);
  return fraction<T>{(lhs.m_num / gcd1) * (rhs.m_num / gcd2), (lhs.m_den / gcd2) * (rhs.m_den / gcd1)};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator*(const fraction<T>& lhs, const T rhs) noexcept
{
  const T gcd = std::gcd(lhs.m_den, rhs);
  return fraction<T>{lhs.m_num * (rhs / gcd), lhs.m_den / gcd};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator*(const T lhs, const fraction<T>& rhs) noexcept
{ return rhs * lhs; }

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator/(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{
  T gcd1 = std::gcd(lhs.m_num, rhs.m_num);
  if (rhs.m_num < 0) gcd1 = -gcd1;
  const T gcd2 = std::gcd(lhs.m_den, rhs.m_den);
  return fraction<T>{(lhs.m_num / gcd1) * (rhs.m_den / gcd2), (lhs.m_den / gcd2) * (rhs.m_num / gcd1)};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator/(const fraction<T>& lhs, const T rhs) noexcept
{
  T gcd = std::gcd(lhs.m_num, rhs);
  if (rhs < 0) gcd = -gcd;
  return fraction<T>{lhs.m_num / gcd, lhs.m_den * (rhs / gcd)};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator/(const T lhs, const fraction<T>& rhs) noexcept
{
  T gcd = std::gcd(lhs, rhs.m_num);
  if (rhs.m_num < 0) gcd = -gcd;
  return fraction<T>{rhs.m_den * (lhs / gcd), rhs.m_num / gcd};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator%(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{
  const T lcm = std::lcm(lhs.m_den, rhs.m_den);
  return fraction<T>((lhs.m_num * (lcm / lhs.m_den)) % (rhs.m_num * (lcm / rhs.m_den)), lcm);
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator%(const fraction<T>& lhs, const T rhs) noexcept
{
  return fraction<T>{lhs.m_num % (lhs.m_den * rhs), lhs.m_den};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::operator%(const T lhs, const fraction<T>& rhs) noexcept
{
  return fraction<T>((lhs * rhs.m_den) % rhs.m_num, rhs.m_den);
}

template<typename T>
inline constexpr bool ctp::operator==(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{
  return lhs.m_num == rhs.m_num && lhs.m_den == rhs.m_den;
}

template<typename T>
inline constexpr bool ctp::operator==(const fraction<T>& lhs, const T rhs) noexcept
{
  return lhs.m_num == rhs && lhs.m_den == 1;
}

template<typename T>
inline constexpr bool ctp::operator==(const T lhs, const fraction<T>& rhs) noexcept
{ return rhs == lhs; }

template<typename T>
inline constexpr bool ctp::operator!=(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{ return !(lhs == rhs); }

template<typename T>
inline constexpr bool ctp::operator!=(const fraction<T>& lhs, const T rhs) noexcept
{ return !(lhs == rhs); }

template<typename T>
inline constexpr bool ctp::operator!=(const T lhs, const fraction<T>& rhs) noexcept
{ return !(lhs == rhs); }

template<typename T>
inline constexpr bool ctp::operator<(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{
  const T lcm = std::lcm(lhs.m_den, rhs.m_den);
  return lhs.m_num * (lcm / lhs.m_den) < (lcm / rhs.m_den) * rhs.m_num;
}

template<typename T>
inline constexpr bool ctp::operator<(const fraction<T>& lhs, const T rhs) noexcept
{
  return lhs.m_num < lhs.m_den * rhs;
}

template<typename T>
inline constexpr bool ctp::operator<(const T lhs, const fraction<T>& rhs) noexcept
{ return rhs > lhs; }

template<typename T>
inline constexpr bool ctp::operator<=(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{
  const T lcm = std::lcm(lhs.m_den, rhs.m_den);
  return lhs.m_num * (lcm / lhs.m_den) <= (lcm / rhs.m_den) * rhs.m_num;
}

template<typename T>
inline constexpr bool ctp::operator<=(const fraction<T>& lhs, const T rhs) noexcept
{
  return lhs.m_num <= lhs.m_den * rhs;
}

template<typename T>
inline constexpr bool ctp::operator<=(const T lhs, const fraction<T>& rhs) noexcept
{ return rhs >= lhs; }

template<typename T>
inline constexpr bool ctp::operator>(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{ return rhs < lhs; }

template<typename T>
inline constexpr bool ctp::operator>(const fraction<T>& lhs, const T rhs) noexcept
{ return !(lhs <= rhs); }

template<typename T>
inline constexpr bool ctp::operator>(const T lhs, const fraction<T>& rhs) noexcept
{ return rhs < lhs; }

template<typename T>
inline constexpr bool ctp::operator>=(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{ return rhs <= lhs; }

template<typename T>
inline constexpr bool ctp::operator>=(const fraction<T>& lhs, const T rhs) noexcept
{ return !(lhs < rhs); }

template<typename T>
inline constexpr bool ctp::operator>=(const T lhs, const fraction<T>& rhs) noexcept
{ return rhs <= lhs; }

template<typename T>
inline constexpr ctp::fraction<T> ctp::abs(const fraction<T>& x) noexcept
{
  return fraction<T>{std::abs(x.m_num), x.m_den};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::fabs(const fraction<T>& x) noexcept
{
  return abs(x);
}

template<typename T>
inline ctp::fraction<T> ctp::modf(const fraction<T>& x, fraction<T>* const intpart) noexcept
{
  *intpart = fraction<T>{x.m_num / x.m_den};
  return fraction<T>{x.m_num % x.m_den, x.m_den};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::ceil(const fraction<T>& x) noexcept
{
  if (x.num() > 0) return fraction<T>((x.num() - 1) / x.den() + 1);
  else return fraction<T>(x.num() / x.den());
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::floor(const fraction<T>& x) noexcept
{
  if (x.num() >= 0) return fraction<T>(x.num() / x.den());
  else return fraction<T>((x.num() + 1) / x.den() - 1);
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::fmod(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{ return lhs % rhs; }

template<typename T>
inline constexpr ctp::fraction<T> ctp::trunc(const fraction<T>& x) noexcept
{
  return fraction<T>(x.m_num / x.m_den);
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::round(const fraction<T>& x) noexcept
{
  fraction<T> frac;
  fraction<T> res = modf(x, &frac);
  const T half = (frac.den() - 1) / 2; 
  if (frac.num() > half) ++res;
  else if (frac.num() < -half) --res;
  return res;
}

template<typename T>
inline constexpr long ctp::lround(const fraction<T>& x) noexcept
{ return static_cast<long>(round(x)); }

template<typename T>
inline constexpr long long ctp::llround(const fraction<T>& x) noexcept
{ return static_cast<long long>(round(x)); }

template<typename T>
inline constexpr ctp::fraction<T> ctp::copysign(const fraction<T>& x, const fraction<T>& y) noexcept
{
  return fraction<T>{(x.m_num >= 0) == (y.m_num >= 0) ? x.m_num : -x.m_num, x.m_den};
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::fma(const fraction<T>& x, const fraction<T>& y, const fraction<T>& z) noexcept
{
  return x * y + z;
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::fdim(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{
  const fraction<T> res = lhs - rhs;
  if (res < 0) res = fraction<T>();
  return res;
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::fmax(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{
  return lhs >= rhs ? lhs : rhs;
}

template<typename T>
inline constexpr ctp::fraction<T> ctp::fmin(const fraction<T>& lhs, const fraction<T>& rhs) noexcept
{
  return lhs <= rhs ? lhs : rhs;
}


template<typename T>
inline constexpr ctp::fraction<T>::fraction(const T num) noexcept
  : m_num(num)
{ }

template<typename T>
inline constexpr ctp::fraction<T>::fraction(const T num, const T den) noexcept
{
  T gcd = std::gcd(num, den);
  if (den < 0) gcd = -gcd;
  m_num = num / gcd;
  m_den = den / gcd;
}

template<typename T>
inline constexpr ctp::fraction<T>::fraction(const std::initializer_list<T> il) noexcept
  : m_num(il.size() > 0 ? *il.begin() : T()), m_den(il.size() > 1 ? *std::next(il.begin()) : T(1))
{ }

template<typename T> template<typename U>
inline constexpr ctp::fraction<T>::fraction(const fraction<U>& other) noexcept
  : m_num(other.num), m_den(other.den)
{ }

template<typename T>
inline constexpr T ctp::fraction<T>::num() const noexcept
{ return m_num; }

template<typename T>
inline constexpr T ctp::fraction<T>::den() const noexcept
{ return m_den; }

template<typename T>
inline constexpr ctp::fraction<T>::operator bool() const noexcept
{ return m_num != 0; }

template<typename T>
inline constexpr ctp::fraction<T>::operator int() const noexcept
{ return m_num / m_den; }

template<typename T>
inline constexpr ctp::fraction<T>::operator long() const noexcept
{ return m_num / m_den; }

template<typename T>
inline constexpr ctp::fraction<T>::operator long long() const noexcept
{ return m_num / m_den; }

template<typename T>
inline constexpr ctp::fraction<T>::operator unsigned() const noexcept
{ return m_num / m_den; }

template<typename T>
inline constexpr ctp::fraction<T>::operator unsigned long() const noexcept
{ return m_num / m_den; }

template<typename T>
inline constexpr ctp::fraction<T>::operator unsigned long long() const noexcept
{ return m_num / m_den; }

template<typename T>
inline constexpr ctp::fraction<T>::operator float() const noexcept
{ return float(m_num) / float(m_den); }

template<typename T>
inline constexpr ctp::fraction<T>::operator double() const noexcept
{ return double(m_num) / double(m_den); }

template<typename T>
inline constexpr ctp::fraction<T>::operator long double() const noexcept
{ return static_cast<long double>(m_num) / static_cast<long double>(m_den); }

template<typename T>
inline constexpr ctp::fraction<T> ctp::fraction<T>::inverse() const noexcept
{
  return fraction{m_num < 0 ? -m_den : m_den, m_num < 0 ? -m_num : m_num};
}

template<typename T>
inline ctp::fraction<T>& ctp::fraction<T>::invert() noexcept
{
  return (*this = inverse());
}

template<typename T>
inline ctp::fraction<T>& ctp::fraction<T>::operator++() noexcept
{
  m_num += m_den;
  return *this;
}

template<typename T>
inline ctp::fraction<T> ctp::fraction<T>::operator++(int) noexcept
{
  const fraction ret(*this);
  ++*this;
  return ret;
}

template<typename T>
inline ctp::fraction<T>& ctp::fraction<T>::operator--() noexcept
{
  m_num -= m_den;
  return *this;
}

template<typename T>
inline ctp::fraction<T> ctp::fraction<T>::operator--(int) noexcept
{
  const fraction ret(*this);
  --*this;
  return ret;
}

template<typename T>
inline ctp::fraction<T>& ctp::fraction<T>::operator+=(const T x) noexcept
{
  return (*this = *this + x);
}

template<typename T>
inline ctp::fraction<T>& ctp::fraction<T>::operator+=(const fraction& other) noexcept
{
  return (*this = *this + other);
}

template<typename T>
inline ctp::fraction<T>& ctp::fraction<T>::operator-=(const T x) noexcept
{
  return (*this = *this - x);
}

template<typename T>
inline ctp::fraction<T>& ctp::fraction<T>::operator-=(const fraction& other) noexcept
{
  return (*this = *this - other);
}

template<typename T>
inline ctp::fraction<T>& ctp::fraction<T>::operator*=(const T x) noexcept
{
  return (*this = *this * x);
}

template<typename T>
inline ctp::fraction<T>& ctp::fraction<T>::operator*=(const fraction& other) noexcept
{
  return (*this = *this * other);
}

template<typename T>
inline ctp::fraction<T>& ctp::fraction<T>::operator/=(const T x) noexcept
{
  return (*this = *this / x);
}

template<typename T>
inline ctp::fraction<T>& ctp::fraction<T>::operator/=(const fraction& other) noexcept
{
  return (*this = *this / other);
}

#endif
