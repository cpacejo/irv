CPPFLAGS = -std=c++17 -Wall -Wextra -Werror -Wpedantic -pedantic-errors -ftrapv -O2

.PHONY: all

all: irv

OBJS = irv.o fraction.o getoptpp.o

irv: $(OBJS)
	c++ $(LDFLAGS) -o $@ $(OBJS)

fraction.o: fraction.h
getoptpp.o: getoptpp.h
irv.o: fraction.h getoptpp.h
