#ifndef CTP_GETOPTPP_H
#define CTP_GETOPTPP_H

#include <initializer_list>
#include <iterator>
#include <string>

namespace ctp
{
  enum class has_arg { no_argument = 0, required_argument = 1, optional_argument = 2 };

  struct option
  {
    std::string name;
    enum has_arg has_arg = has_arg::no_argument;
    int* flag = nullptr;
    int val = 0;
  };

  class getopt
  {
  public:
    class iterator
    {
    public:
      struct value_type
      {
        int opt;
        int ind;
        char* arg;
      };

      using difference_type = void;
      using reference = value_type;
      using pointer = const value_type*;
      using iterator_category = std::input_iterator_tag;

      iterator& operator++();
      iterator operator++(int);

      bool operator==(const iterator&) noexcept;
      bool operator!=(const iterator&) noexcept;

      reference operator*();
      pointer operator->();

    private:
      constexpr iterator() noexcept;
      iterator(const class getopt*) noexcept;

      friend class getopt;

      const class getopt* getopt;
      value_type value;
    };

    getopt(int argc, char* argv[],
      std::string optstring,
      std::initializer_list<option> = {});

    iterator begin() const;
    constexpr iterator end() const;

  private:
    friend class iterator;

    int argc;
    char** argv;
    const std::string optstring;
  };
}


// PRIVATE

inline bool ctp::getopt::iterator::operator==(const iterator& other) noexcept
{ return value.opt == -1 && other.value.opt == -1; }

inline bool ctp::getopt::iterator::operator!=(const iterator& other) noexcept
{ return !(*this == other); }

inline ctp::getopt::iterator::reference ctp::getopt::iterator::operator*()
{ return value; }

inline ctp::getopt::iterator::pointer ctp::getopt::iterator::operator->()
{ return &value; }

inline constexpr ctp::getopt::iterator::iterator() noexcept
  : getopt(nullptr), value{-1, 0, nullptr}
{ }

inline constexpr ctp::getopt::iterator ctp::getopt::end() const
{ return iterator(); }

#endif
